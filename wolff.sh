#!/bin/bash

LATTICE_N=(16 32 64 128)
N=${LATTICE_N[$1]}

./main -wolff -threads 4 -max 1000000 -burnin 100000 -sweep 10000 -c 1.0 -m 0.0 -start 0.1 -end 5.0 -step 0.1 -n ${N} -o wolff_${N}.dat > wolff_${N}.conf
