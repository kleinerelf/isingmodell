#!/bin/bash

LATTICE_N=(16 32 64 128)
N=${LATTICE_N[$1]}

( set -x
./main -threads 4 -max 10000000 -burnin 1000000 -sweep 50000 -c 1.0 -m 0.0 -start 0.1 -end 5.0 -step 0.1 -n ${N} -o metropolis_${N}.dat | tee metropolis_${N}.conf )
