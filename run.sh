#!/bin/bash

if [ "$2" != "" ]
then
    JOBS=$2
else
    JOBS=3
fi


if [ "$1" == "wolff" ]
then
    seq 0 3 | parallel -j${JOBS} ./wolff.sh {}
else
    seq 0 3 | parallel -j${JOBS} ./metropolis.sh {}
fi
