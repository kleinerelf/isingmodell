=====================================================================
Ising-Modell: Implementierung durch Metropolis- und Wolff-Algorithmus
=====================================================================
:Info: <https://bitbucket.org/kleinerelf/isingmodell/>
:Autor: Simon Braß <simon.brass@tu-dortmund.de>
:Datum: $Datum: 27. Oktober 2015 $
:Version: v0.5
:Beschreibung: Kurz-Anleitung zur Benutzung der Implementierung des Metropolis bzw. Wolff-Algorithmus in Fortran 2008.

.. warning:: Es wird ein Fortran Kompiler benötigt, der den Standard 2008 unterstützt.

.. contents:: Inhaltsverzeichnis
             
Installation
============

Standard: Fortran 2008

Kompilieren:
............
Vorraussetzung: gfortran >=4.8

.. code-block:: bash

   make

Test:
.....
Kompilieren

.. code-block:: bash

   make check

Clean:
......
Löschen aller Binaries und Laufzeit-Dateien von gfortran.

.. code-block:: bash

   make clean

Benutzung
=========

Help
....

.. code-block:: bash

   main -h
   main --help

Ausgabe:

.. code-block:: bash

   main
   [-o STRING]             # defualt_value = out.dat
   [-wolff]                # default: Metropolis Algorithmus
   [-c REAL]               # default value = 1.0
   [-m REAL]               # default value = 0.0
   [-n INTEGER]            # default value = 100
   [-max INTEGER]          # default_value = 10000
   [-burnin INTEGER]       # default_value = 1000
   [-sweep INTEGER]        # default_value = 200
   [-start REAL]           # default_value = 0.25
   [-end REAL]             # default_value = 5.0
   [-step REAL]            # default_value = 0.25
   [-threads INTEGER]      # default_value = 3
   [-h|-help|--help]|      # help flag=.F.
   [-v|-version|--version] # version flag=.F.

Version
.......

.. code-block:: bash

   main -v
   main --version

Ausgabe:

.. code-block:: bash

   LatticeGas. Simon Brass und Markus Kurbel. v0.4

Beispiele
=========

Beispiele mit dem Metropolis-Algorithmus.

Single-Thread
.............

Simulation des Ising-Modell für kT im Intervall [1.5:3.0] in 0.1 Schritten.

.. code-block:: bash

   main -start 1.5 -end 3.0 -step 0.1 -threads 1

Länge der Simulation auf 25,000 und Einlaufzeit auf 5,000 erhöhen.

.. code-block:: bash

   main -start 1.5 -end 3.0 -step 0.1 -max 25000 -burnin 5000 -threads 1

Änderung der Kopplungskonstante auf 0.5 und des Magnetfeldes auf 0.1.

.. code-block:: bash

   main -start 1.5 -end 3.0 -step 0.1 -max 25000 -burnin 5000 -c 0.5 -m 0.1 -threads 1

Latticegröße auf 32 x 32 ändern und Ausgabe in lattice_32.dat ändern.

.. code-block:: bash

   main -start 1.5 -end 3.0 -step 0.1 -max 25000 -burnin 5000 -c 0.5 -m 0.1 -n 32 -o lattice_32.dat -threads 1

Änderung der MC-Sweep Dauer auf 1000. Anzahl der Spin-Flips bis eine neue Messung stattfindet.

.. code-block:: bash

   main -start 1.5 -end 3.0 -step 0.1 -max 25000 -burnin 5000 -sweep 1000 -c 0.5 -m 0.1 -n 32 -o lattice_32.dat -threads 1

Multi-Threading
...............

Durch Angabe einer Thread-Anzahl wird die Simulation **dynamisch** aufgeteilt.

.. code-block:: bash

   main -threads 4

Beispiel mit nohup
..................

Beispiel mit **nohup** für ein 16 x 16 Sytem.

.. code-block:: bash

   nohup ./main -start 0.1 -end 5.0 -step 0.01 -max 14000000 -burnin 1400000 -sweep 700000 -c 1.0 -m 0.0 -threads 4 -n 16 \
		-o metropolis_16.dat > metropolis_16.conf 2> metropolis_16.err < /dev/null &

Wolff-Algorithmus
.................

Anstelle des Metropolis kann der Wolff-Algorithmus verwendet werden.
Die Übergabe der Parameter bleibt identisch.

.. code-block:: bash

   main -wolff [...]

Plotten
.......

Zum Auswerten und Plotten liegen die Skripte: **run.sh**, **run_wolff.sh**, **metropolis.sh**, **wolff.sh** bereit.
Ein GNUplot-Skript liegt im Unterordner *result/* mit welchem Ergebnisse für Metropolis- und Wolff-Algorithmen für die Systemgrößen
16 bis 128 geplottet werden können. (Siehe dazu das GNUplot-Skript.)

Quellcode
=========

.. role:: fortran(code)
   :language: fortran

Der zugrundeliegende Quellcode ist mithilfe von Fortran 2008 entstanden und unterstützt in vollem Maße Objektorientiertes Programmieren.

Als Beispiele wird ein abstrakter Datentyp :fortran:`model_t` definiert, welcher verschiedene *deferred type-bound procedures* bereit hält.
Diese dienen als Platzhalter für die Implementierung, bzw. für die Modellspezifikationen.

.. code-block:: fortran
                
   type, abstract :: modell_t
     integer :: sys_len

     real(dp) :: energy, &
          mag_field = 0._dp, &
          couple_const = 1._dp

     type(rand_t) :: rand_gen
   contains
     procedure(clean), deferred, pass, public :: clean
     procedure(get_energy), deferred, pass, public :: get_energy
     procedure(get_magnetization), deferred, pass, public :: get_magnetization
     procedure(get_correlation), deferred, pass, public :: get_correlation
     procedure(flip_spin), deferred, pass, public :: flip_spin
     procedure(print_grid), deferred, pass, public :: print_grid
   end type modell_t
   
Dieser Datentyp definiert die grundlegenden Funktionen für eines von verschiedenen Modellen zur Berechnung des Ising-Modell.
Die Implementierung des abstrakten Datentypes nutzt, dass die eigentliche Modellvariable polymorph definiert ist.
Das polymorphe Objekt wird dann von Fortran typ-sicher gecastet und es kann auf die verschiedenen Prozeduren nutzbar zugegriffen werden.

.. code-block:: fortran

   type modell_impl_t
     class(modell_t), allocatable :: impl
   contains
     procedure, public :: clean => clean
     procedure, public :: flip_spin => flip_spin
     procedure, public :: get_energy => get_energy
     procedure, public :: get_correlation => get_correlation
     procedure, public :: get_magnetization => get_magnetization
     procedure, public :: print_grid => print_grid
   end type modell_impl_t
  
Die Implementation wird typ-spezifischen allokiert mithilfe der source-Option von allocate.

.. code-block:: fortran

   select case (name)
   case("wolff")
       allocate(constructor%impl, source=wolff_t(n, mag_field, coupling))
   case("metropolis")
       allocate(constructor%impl, source=metropolis_t(n, mag_field, coupling))
   case default
       write(*, "(A)") "Passendes Modell nicht gefunden."
       stop
   end select

Die Verwendung des eigentlich Modells wird von der Implementation übernommen. Hierfür wird das polymorphe Modell-Objekt typ-sicher durch einen :fortran:`select type` nutzbar bereitgestellt.

.. code-block:: fortran

   select type (modell => self%impl)
   class default
      call modell%flip_spin(kT)
   end select

Ein entsprechendes Modell wird als erweiterter Datentyp von :fortran:`modell_t` definiert.
Als Beispiel hier dient der Datentyp :fortran:`metropolis_t` für den Metropolis Algorithmus.

.. code-block:: fortran

   type, extends(modell_t) :: metropolis_t
     private
     integer, dimension(:, :), allocatable :: grid
   contains
      procedure, public :: clean => clean
      procedure, public :: flip_spin => flip_spin
      procedure, public :: get_energy => get_energy
      procedure, public :: get_correlation => get_correlation
      procedure, public :: get_magnetization => get_magnetization
      procedure, public :: print_grid => print_grid

     procedure, private :: calc_energy
     procedure, private :: cyclic_coord
  end type metropolis_t

Wobei beliebig viele weitere Prozeduren oder interne Datenstrukturen angelegt werden können neben den festvorgegebenen Strukturen durch den abstrakten Datentyp :fortran:`modell_t`.

Durch die Nutzung eines abstrakten Datentypes ist es möglich mit wenigen Anpassungen am Quellcode ein weiteres Modell zu implementieren.
Der eigentliche Aufruf des Modells im Hauptprogramm bleibt komplett unangetastet und steht völlig autark vom Modell und seiner spezifischen Implementierung in Fortran, bzw. in C(++) durch Binding.

Die Benutzung eines Modells im Hauptprogramm findet durch die Modellimplementierung statt.

.. code-block:: fortran

   allciocate(modell(n_steps))

  !$OMP PARALLEL DO SCHEDULE(DYNAMIC, 8)&
  !$OMP PRIVATE (j, k)
  do i = 1, n_steps
     write(*, "(A, F12.6)") "Messung für kT: ", kT(i)
     modell(i) = modell_impl_t(trim(modell_name), sys_len, mag_field, coupling)

     ! Einlaufzeit
     do j = 1, burnin
        call modell(i)%flip_spin(kT(i))
     end do

     k = 0

     ! Messung
     do j = burnin, max_iter
        call modell(i)%flip_spin(kT(i))

        k = k + 1

        if (k .GT. sweep) then
           call energy(i)%update(abs(modell(i)%get_energy()))
           call mag(i)%update(abs(modell(i)%get_magnetization()))
           k = 0
        end if
     end do

     ! Ausgabe
     ! ...
     call modell(i)%clean()
  end do
