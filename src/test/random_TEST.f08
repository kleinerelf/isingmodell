program random_TEST
  use precession_mod, only: dp
  use rand_mod

  type(rand_t) :: rand_gen
  real(dp) :: scalar
  real(dp), dimension(4) :: array
  real(dp), dimension(2, 2) :: array_2d

  write (*, "(A)") "!****************************************"
  write (*, "(A)") "!TEST: random.f08"
  write (*, "(A)") "!****************************************"

  rand_gen = rand_t()

  call rand_gen%rand(scalar)
  call rand_gen%rand(array)
  call rand_gen%rand(array_2d)

  write(*, "(A, F12.6)") "Skalar: ", scalar
  write(*, "(A, F12.6, F12.6, F12.6, F12.6)") "Array: ", array(1), array(2), array(3), array(4)
  write(*, "(A)") "Array 2D:"
  write(*, "(F12.6, F12.6)") array_2d(1, 1), array_2d(1, 2)
  write(*, "(F12.6, F12.6)") array_2d(2, 1), array_2d(2, 2)
end program random_TEST
  
