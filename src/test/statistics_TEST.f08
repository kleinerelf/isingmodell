
! [[file:~/isingmodell/src/test/statistics_TEST.org::*Header][Header:1]]

program statistics_TEST
  use precession_mod, only: dp
  use statistics_mod
  use unit_test_mod
  implicit none

! Header:1 ends here

! [[file:~/isingmodell/src/test/statistics_TEST.org::*Intialization%20of%20the%20Unit%20test][Intialization\ of\ the\ Unit\ test:1]]

  type(unit_test_t) :: results

  character(len=:), allocatable :: name, description

  results = unit_test_t()

! Intialization\ of\ the\ Unit\ test:1 ends here

! [[file:~/isingmodell/src/test/statistics_TEST.org::*Intialization%20of%20the%20Unit%20test][Intialization\ of\ the\ Unit\ test:1]]

  write (*, "(A)") "!****************************************"
  write (*, "(A)") "!File: statistics.f08"
  write (*, "(A)") "!****************************************"

  name = "[Statistics] Constructor"
  description = "Test for constructor overloading."
  call test(statistics_test_constructor, name, description, results)

  name = "[Statistics] Update Real"
  description = "mean_var_t%update => mean_variance_real"
  call test(statistics_test_real, name, description, results)

  name = "[Statistics] Update Array"
  description = "mean_var_t%update => mean_variance_array"
  call test(statistics_test_array, name, description, results)

  call results%write()
contains

! Intialization\ of\ the\ Unit\ test:1 ends here

! [[file:~/isingmodell/src/test/statistics_TEST.org::*Constructor:%20Add%20more%20information%20on%20this%20defintion.][Constructor:\ Add\ more\ information\ on\ this\ defintion\.:1]]

    subroutine statistics_test_constructor(success)
      logical, intent(out) :: success

      type(mean_var_t) :: stat
      character(len=:), allocatable :: description

      success = .true.
      stat = mean_var_t()

      description = "Constructor with predefined mean."
      success = success .and. nearly_equal(0.0_dp, stat%get_mean())
      call assert_equal(0.0_dp, stat%get_mean(), description)

      description = "Constructor with predefined variance."
      success = success .and. nearly_equal(0.0_dp, stat%get_var())
      call assert_equal(0.0_dp, stat%get_var(), description)

      description = "Constructor with predefined count."
      success = success .and. (10 == stat%get_n())
      call assert_equal(0, stat%get_n(), description)

      write(*, "(A, F12.6, A, F12.6, A, I0)") "Mean: ", stat&
           &%get_mean(), " Variance: ", stat%get_var(), " Count: ", stat&
           &%get_n()

! Constructor:\ Add\ more\ information\ on\ this\ defintion\.:1 ends here

! [[file:~/isingmodell/src/test/statistics_TEST.org::*Constructor:%20Add%20more%20information%20on%20this%20defintion.][Constructor:\ Add\ more\ information\ on\ this\ defintion\.:1]]

      stat = mean_var_t(1.0_dp, 0.5_dp, 10)

      description = "Constructor with predefined mean."
      success = success .and. nearly_equal(1.0_dp, stat%get_mean())
      call assert_equal(1.0_dp, stat%get_mean(), description)

      description = "Constructor with predefined variance."
      success = success .and. nearly_equal(0.5_dp, stat%get_var())
      call assert_equal(0.5_dp, stat%get_var(), description)

      description = "Constructor with predefined count."
      success = success .and. (10 == stat%get_n())
      call assert_equal(10, stat%get_n(), description)

      write(*, "(A, F12.6, A, F12.6, A, I0)") "Mean: ", stat&
           &%get_mean(), " Variance: ", stat%get_var(), " Count: ", stat&
           &%get_n()
    end subroutine statistics_test_constructor

! Constructor:\ Add\ more\ information\ on\ this\ defintion\.:1 ends here

! [[file:~/isingmodell/src/test/statistics_TEST.org::*Test%20update%20algorithm%20for%20real][Test\ update\ algorithm\ for\ real:1]]

subroutine statistics_test_real(success)
  logical, intent(out) :: success

  type(mean_var_t) :: stat

  success = .false.

  call stat%update(1.0_dp)
  call stat%update(4.0_dp)
  call stat%update(1.0_dp)
  call stat%update(2.0_dp)
  call stat%update(-1.0_dp)

  write(*, "(A, F12.6, A, F12.6, A, I0)") "Mean: ", stat&
       &%get_mean(), " Variance: ", stat%get_var(), " Count: ", stat&
       &%get_n()

  success = .true.
end subroutine statistics_test_real

! Test\ update\ algorithm\ for\ real:1 ends here

! [[file:~/isingmodell/src/test/statistics_TEST.org::*Test%20update%20algorithm%20for%20real%20arrays][Test\ update\ algorithm\ for\ real\ arrays:1]]

  subroutine statistics_test_array(success)
    logical, intent(out) :: success

    type(mean_var_t) :: stat

    success = .false.

    call stat%update((/1.5_dp, 2.5_dp, 3.5_dp/))

    write(*, "(A, F12.6, A, F12.6, A, I0)") "Mean: ", stat&
         &%get_mean(), " Variance: ", stat%get_var(), " Count: ", stat&
         &%get_n()

    success = .true.
  end subroutine statistics_test_array
end program statistics_TEST

! Test\ update\ algorithm\ for\ real\ arrays:1 ends here
