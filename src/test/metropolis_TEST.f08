program metropolis_TEST
  use precession_mod, only: dp
  use metropolis_mod
  use statistics_mod

  type(metropolis_t) :: metropolis
  type(mean_var_t) :: energy, mag

  integer :: i

  write (*, "(A)") "!****************************************"
  write (*, "(A)") "!TEST: metropolis.f08"
  write (*, "(A)") "!****************************************"

  write(*, "(A)") "#**************************************************"
  write(*, "(A, I0)") "# Systemgröße: ", 10
  write(*, "(A, F12.6)") "# kT: ", 1.5
  write(*, "(A, F12.6)") "# Magnetfeld: ", 1.0
  write(*, "(A, F12.6)") "# Kopplung: ", 0.5
  write(*, "(A)") "#**************************************************"

  metropolis = metropolis_t(10, 1.0_dp, 0.5_dp)
  
  call metropolis%print_grid()

  do i = 1, 100
     call metropolis%flip_spin(1.5_dp)
     call energy%update(metropolis%get_energy())
     call mag%update(metropolis%get_magnetization())
  end do

  do i = 1, 10
     write(*, "(I0, F12.6)") i, metropolis%get_correlation(1, i)
  end do

  write(*, "(A)") "#**************************************************"
  write(*, "(A, F12.6, F12.6)") "# Mittlere Energie: ", energy%get_mean(), sqrt(energy%get_var())
  write(*, "(A, F12.6, F12.6)") "# Mittlere Magnetisiering: ", mag%get_mean(), sqrt(mag%get_var())
  write(*, "(A)") "#**************************************************"
  call metropolis%print_grid()

  call metropolis%clean()
end program metropolis_TEST
