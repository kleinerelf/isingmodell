program linked_list_TEST
  use list_mod
  use unit_test_mod
  implicit none

  type(unit_test_t) :: results

  character(len=:), allocatable :: name, description

  results = unit_test_t()

  write (*, "(A)") "!****************************************"
  write (*, "(A)") "!TEST: linked_list.f08"
  write (*, "(A)") "!****************************************"

  name = "[List] Construtor"
  description = "Construct list of arrays with predefined length"
  call test(test_add_element, name, description, results)

  name = "[List] Add and get element"
  description = "Add element consecutive and get consecutive"
  call test(test_add_element, name, description, results)

  name = "[List] Search element"
  description = "Search elements consecutive"
  call test(test_search_element, name, description, results)

  call results%write()
contains
  subroutine test_constructor(success)
    logical, intent(out) :: success

    type(list_t) :: list
    integer, parameter :: list_len = 10
    character(len=:), allocatable :: description

    success = .true.

    list = list_t(list_len)
    success = success .and.( list%get_len() == list_len )
    description = "Constructor: List length"
    call assert_equal(list%get_len(), list_len, description)
  end subroutine test_constructor

  subroutine test_add_element(success)
    logical, intent(out) :: success

    type(list_t) :: list
    integer, parameter :: list_len = 10

    integer :: i

    list = list_t(list_len)
    success = .true.
    do i=1, list_len
       call list%add([i, i+1])
    end do

    do i=1, list_len
       success = success .and. all((list%get(i) == [i, i+1]))
    end do
  end subroutine test_add_element

  subroutine test_search_element(success)
    logical, intent(out) :: success

    type(list_t) :: list
    integer, parameter :: list_len = 10

    integer :: i

    list = list_t(list_len)
    success = .true.
    do i=1, list_len
       call list%add([i, i+1])
    end do

    do i=1, list_len
       success = success .and. list%search([i, i+1])
    end do
  end subroutine test_search_element
end program linked_list_TEST
