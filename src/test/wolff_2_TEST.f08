program wolff_TEST
  use precession_mod, only: dp
  use wolff_mod
  use statistics_mod

  integer, parameter :: N = 10
  real(dp), parameter :: kT = 1.5_dp, &
       & m = 0.0_dp, &
       & c = 1.0_dp

  type(wolff_t) :: wolff

  integer, dimension(N, N) :: grid

  integer :: i

  write (*, "(A)") "#****************************************"
  write (*, "(A)") "#TEST: wolff.f08"
  write (*, "(A)") "#****************************************"

  write(*, "(A)") "#**************************************************"
  write(*, "(A, I0)") "# Systemgröße: ", N
  write(*, "(A, F12.6)") "# kT: ", kT
  write(*, "(A, F12.6)") "# Magnetfeld: ", m
  write(*, "(A, F12.6)") "# Kopplung: ", c
  write(*, "(A)") "#**************************************************"

  wolff = wolff_t(N, m, c)

  do i = 1, 100
     call wolff%flip_spin(kT)
  end do

  write(*, "(A)") "#**************************************************"
  do i = 1, 1000
     write(*, "(A)") ""
     write(*, "(A)") ""
     grid = wolff%get_grid()
     call wolff%flip_spin(kT)
     call wolff%diff_grid(grid)
  end do
  write(*, "(A)") "#**************************************************"

  call wolff%clean()
end program wolff_TEST
