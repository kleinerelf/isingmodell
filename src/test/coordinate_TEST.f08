program main
  use coord_impl_mod

  implicit none

  type(coord_grid_t) :: p
  integer, dimension(:), allocatable :: coords

  coords = [5, 10]
  p = coord_grid_t(size(coords), coords)
  print *, "Init: ", coords
  call p%get(coords)
  if (all(coords == [5, 10])) then
     print *, "Coord-Check: PASS"
  else
     print *, "Coord-Check: FAIL"
  end if

  call p%print()
  print *, "=================================================="

  coords = [5, 13, 20]
  print *, "New: ", coords
  if (p%set(coords)) then
     print *, "Set new Coords: FAIL"
  else
     print *, "Set new Coords: PASS"
  end if

  call p%print()
  print *, "=================================================="

  coords = [2, 5]
  print *, "New: ", coords
  if (p%set(coords)) then
     print *, "Set new Coords: PASS"
  else
     print *, "Set new Coords: FAIL"
  end if

  call p%print()
end program main
