program wolff_TEST
  use precession_mod, only: dp
  use wolff_mod
  use statistics_mod

  type(wolff_t) :: wolff
  type(mean_var_t) :: energy, mag

  integer :: i

  write (*, "(A)") "#****************************************"
  write (*, "(A)") "#TEST: wolff.f08"
  write (*, "(A)") "#****************************************"

  write(*, "(A)") "#**************************************************"
  write(*, "(A, I0)") "# Systemgröße: ", 10
  write(*, "(A, F12.6)") "# kT: ", 1.5
  write(*, "(A, F12.6)") "# Magnetfeld: ", 0.0
  write(*, "(A, F12.6)") "# Kopplung: ", 1.0
  write(*, "(A)") "#**************************************************"

  wolff = wolff_t(10, 0.0_dp, 1.0_dp)

  call wolff%print_grid()

  do i = 1, 200
     call wolff%flip_spin(1.5_dp)
     call energy%update(wolff%get_energy())
     call mag%update(wolff%get_magnetization())
  end do

  do i = 1, 10
     write(*, "(I0, F12.6)") i, wolff%get_correlation(1, i)
  end do

  write(*, "(A)") "#**************************************************"
  write(*, "(A, F12.6, F12.6)") "# Mittlere Energie: ", energy%get_mean(), sqrt(energy%get_var())
  write(*, "(A, F12.6, F12.6)") "# Mittlere Magnetisiering: ", mag%get_mean(), sqrt(mag%get_var())
  write(*, "(A)") "#**************************************************"
  call wolff%print_grid()

  call wolff%clean()
end program wolff_TEST
