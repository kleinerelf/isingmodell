module message_mod
  use ISO_fortran_env, only: ERROR_UNIT
  implicit none

  private

  integer, parameter :: debug = 0, warning = 1, error = 2, critical = 3

  type :: message_t
     private
     integer :: level = error
     integer :: report_level = warning

     integer :: debug_c = 0, &
          & warn_c = 0, &
          & error_c = 0, &
          & crit_c = 0
   contains
     procedure, public :: set_report_level => message_set_report_level
     procedure, public :: print => message_print
  end type message_t

  interface message_t
     module procedure construct
  end interface message_t

  type(message_t) :: error_handler = message_t(error, error)

  public :: message_t, error_handler, debug, warning, error, critical
contains

  function construct(level, report_level)
    type(message_t) :: construct
    integer, intent(in) :: level
    integer, intent(in), optional :: report_level

    associate (self => construct)
      self%level = level

      if (present(report_level)) then
         self%report_level = report_level
      end if
    end associate
  end function construct

  subroutine message_set_report_level(self, report_level)
    class(message_t), intent(inout) :: self
    integer, intent(in) :: report_level

    self%report_level = report_level
  end subroutine message_set_report_level

  subroutine message_print(self, message, type)
    class(message_t), intent(inout) :: self
    character(*), intent(in) :: message
    integer, intent(in), optional :: type

    integer :: local_type

    if (present(type)) then
       local_type = type
    else
       local_type = self%level
    end if

    select case (local_type)
    case (debug)
       self%debug_c = self%debug_c + 1
       if (local_type >= self%report_level) write(ERROR_UNIT, *) "[DEBUG]: ", message
    case (warning)
       self%warn_c = self%warn_c + 1
       if (local_type >= self%report_level) write(ERROR_UNIT, *) "[WARNING]: ", message
    case (error)
       self%error_c = self%error_c + 1
       if (local_type >= self%report_level) write(ERROR_UNIT, *) "[ERROR]: ", message
    case (critical)
       self%crit_c = self%crit_c + 1
       if (local_type >= self%report_level) write(ERROR_UNIT, *) "[CRITICAL]: ", message
       stop
    case default
       if (local_type >= self%report_level) write(ERROR_UNIT, *) "[UNKNOWN]: ", message
       stop
    end select
  end subroutine message_print
end module message_mod
