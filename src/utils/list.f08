! Dynamischer Array-Wrapper a la C++ (Liste)
module list_mod
  ! use precession
  implicit none

  private

  type element_t
     integer, dimension(2) :: val = [ 0, 0 ]
  end type element_t

  type :: list_t
     private
     integer :: &
          len = 0, &
          max = 0
     type(element_t), dimension(:), allocatable :: element
   contains
     procedure, public :: add => add_element
     procedure, public :: get => get_val
     procedure, public :: print => print_list
     procedure, public :: search => search_list
     final :: free_list
     procedure, public :: get_len => get_list_len
  end type list_t

  interface list_t
     module procedure construct
  end interface list_t

  public :: list_t
contains
  type(list_t) function construct(n)
    integer, intent(in) :: n

    construct%len = 0
    construct%max = n
    allocate(construct%element(n))
  end function construct

  subroutine add_element(self, val)
    class(list_t) :: self
    integer, dimension(2), intent(in) :: val

    type(element_t), dimension(:), allocatable :: element

    if (self%len .GT. self%max) then
       ! Enlarge Array
       allocate(element(self%len))
       element = self%element

       deallocate(self%element)
       self%max = 2 * self%max
       allocate(self%element(self%max))

       self%element(1:self%len) = element
       deallocate(element)
       ! write(*, *) "list_t%add_element: Array ist voll."
       ! stop
    end if

    self%len = self%len + 1
    self%element(self%len)%val = val
  end subroutine add_element

  function get_val(self, i)
    class(list_t) :: self
    integer, intent(in) :: i

    integer, dimension(2) :: get_val

    get_val = self%element(i)%val
  end function get_val

  subroutine print_list(self)
    class(list_t) :: self

    integer :: i

    do i = 1, self%len
       write(*, *) self%element(i)%val
    end do
  end subroutine print_list

  subroutine free_list(self)
    type(list_t) :: self

    self%len = 0
    deallocate(self%element)
  end subroutine free_list

  logical function search_list(self, item)
    class(list_t) :: self
    integer, dimension(2), intent(in) :: item

    integer :: i

    do i = 1, self%len
       if (all(self%element(i)%val == item)) then
          search_list = .TRUE.
          exit
       end if
    end do
  end function search_list

  integer function get_list_len(self)
    class(list_t) :: self

    ! self%len = size(self%element)
    get_list_len = self%len
  end function get_list_len
end module list_mod
