module statistics_mod
  use precession_mod, only: dp

  implicit none

  private

  type :: mean_var_t
     private
     integer :: n = 0
     real(dp) :: &
          mean = 0.0_dp, &
          variance = 0.0_dp
   contains
     procedure, public :: get_mean
     procedure, public :: get_var
     procedure, public :: get_n
     procedure, public :: set_mean
     procedure, public :: set_var
     procedure, public :: set_n
     procedure, private :: mean_variance_real
     procedure, private :: mean_variance_array
     generic, public :: update => mean_variance_real, mean_variance_array
     procedure, public :: write
  end type mean_var_t

  interface mean_var_t
     module procedure construct
  end interface mean_var_t

  interface assignment(=)
     module procedure assign_mean_var_t
  end interface assignment(=)

  public :: mean_var_t, assignment(=)
contains

  type(mean_var_t) function construct(mean, variance, n)
    integer, intent(in) :: n
    real(dp), intent(in) :: mean, variance

    construct%mean = mean
    construct%variance = variance
    construct%n = n
  end function construct

  subroutine assign_mean_var_t(lhs, rhs)
    type(mean_var_t), intent(out) :: lhs
    type(mean_var_t), intent(in) :: rhs

    call lhs%set_mean(rhs%get_mean())
    call lhs%set_var(rhs%get_var())
    call lhs%set_n(rhs%get_n())
  end subroutine assign_mean_var_t

  real(dp) function get_mean(self)
    class(mean_var_t), intent(in) :: self

    get_mean = self%mean
  end function get_mean

  real(dp) function get_var(self)
    class(mean_var_t), intent(in) :: self

    get_var = self%variance
  end function get_var

  integer function get_n(self)
    class(mean_var_t), intent(in) :: self

    get_n = self%n
  end function get_n

  subroutine set_mean(self, mean)
    class(mean_var_t), intent(inout) :: self
    real(dp), intent(in) :: mean

    self%mean = mean
  end subroutine  set_mean

  subroutine set_var(self, var)
    class(mean_var_t), intent(inout) :: self
    real(dp), intent(in) :: var

    self%variance = var
  end subroutine set_var

  subroutine set_n(self, n)
    class(mean_var_t), intent(inout) :: self
    integer, intent(in) :: n

    self%n = n
  end subroutine  set_n

  subroutine mean_variance_real(self, data)
    class(mean_var_t) :: self
    real(dp), intent(in) :: data

    real(dp) :: delta

    self%n = self%n + 1
    delta = data - self%mean
    self%mean = self%mean + delta / self%n
    self%variance = self%variance + delta * (data - self%mean)

    if( self%n < 2 ) then
       self%variance = 0._dp
    else
       self%variance = self%variance / (self%n - 1)
    end if
  end subroutine mean_variance_real

  subroutine mean_variance_array(self, data)
    class(mean_var_t) :: self
    real(dp), dimension(:), intent(in) :: data

    integer :: i
    real(dp) :: delta

    do i = 1, size(data)
       self%n = self%n + 1
       delta = data(i) - self%mean
       self%mean = self%mean + delta / self%n
       self%variance = self%variance + delta * (data(i) - self%mean)
    end do

    if( self%n < 2 ) then
       self%variance = 0._dp
    else
       self%variance = self%variance / (self%n - 1)
    end if
  end subroutine mean_variance_array

  subroutine write(self)
    class(mean_var_t), intent(in) :: self

    print *, "Mean: ", self%mean
    print *, "Var: ", self%variance
    print *, "n: ", self%n
  end subroutine write
end module statistics_mod
