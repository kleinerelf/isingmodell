module coord_impl_mod
  use coord_mod

  implicit none

  private

  type, extends(coord_t) :: coord_grid_t
     integer, private :: GRID_DIM = 2
   contains
     procedure, public :: get => get_coords
     procedure, public :: get_dim => get_dim
     procedure, public :: print => print_coords
     procedure, public :: set => set_coords

     procedure, public :: move => move_coords
  end type coord_grid_t

  interface coord_grid_t
     module procedure constructor
  end interface coord_grid_t

  integer, dimension(2), public :: &
       & GRID_LEFT = [0, -1],&
       & GRID_RIGHT = [0, 1],&
       & GRID_UP = [1, 0],&
       & GRID_DOWN = [-1, 0]

  public :: coord_grid_t
contains
  type(coord_grid_t) function constructor(coords)
    integer, dimension(:), intent(in) :: coords

    ! For the sake of readibility
    associate (self => constructor)
      if ( .not. self%init ) then
         if (size(coords) /= self%GRID_DIM) then
            print *, "coords_t%construct: Misdimensioned Coordindates."
         end if
         self%init = .true.
         self%dim = self%GRID_DIM
         allocate(self%coords(self%dim), source=coords)
      end if
    end associate
  end function constructor

  subroutine get_coords(self, coords)
    class(coord_grid_t), intent(in) :: self
    integer, dimension(:), allocatable, intent(out) :: coords

    allocate(coords(self%dim), source=self%coords)
  end subroutine get_coords

  integer function get_dim(self)
    class(coord_grid_t), intent(in) :: self

    get_dim = self%dim
  end function get_dim

  logical function move_coords(self, coords)
    class(coord_grid_t), intent(inout) :: self
    integer, dimension(:), allocatable, intent(in) :: coords

    move_coords = .false.

    if (size(coords) /= self%dim) then
       print *, "coord_grid_t%set: Wrong dimension of new coordinates."
       move_coords = .false.
    else
       self%coords = self%coords + coords
       move_coords = .true.
    end if
  end function move_coords

  subroutine print_coords(self)
    class(coord_grid_t), intent(in) :: self

    print *, "Dimension: ", self%dim
    print *, "Coordinates: ", self%coords
  end subroutine print_coords

  logical function set_coords(self, coords)
    class(coord_grid_t), intent(inout) :: self
    integer, dimension(:), allocatable, intent(in) :: coords

    set_coords = .false.

    if (size(coords) /= self%dim) then
       print *, "coord_grid_t%set: Wrong dimenison of new coordinates."
       set_coords = .false.
    else
       self%coords = coords
       set_coords = .true.
    end if
  end function set_coords
end module coord_impl_mod
