module coord_mod
  implicit none

  private
  type, abstract :: coord_t
     integer :: dim = 0
     integer, dimension(:), allocatable :: coords
     logical :: init = .false.
   contains
     procedure(get), deferred, pass, public :: get
     procedure(get_dim), deferred, pass, public :: get_dim
     procedure(print), deferred, pass, public :: print
     procedure(set), deferred, pass, public :: set
  end type coord_t

  abstract interface
     subroutine get(self, coords)
       import coord_t
       class(coord_t), intent(in) :: self
       integer, dimension(:), allocatable, intent(out) :: coords
     end subroutine get

     integer function get_dim(self)
       import coord_t
       class(coord_t), intent(in) :: self
     end function get_dim

     subroutine print(self)
       import coord_t
       class(coord_t), intent(in) :: self
     end subroutine print

     logical function set(self, coords)
       import coord_t
       class(coord_t), intent(inout) :: self
       integer, dimension(:), allocatable, intent(in) :: coords
     end function set
  end interface

  public coord_t
end module coord_mod
