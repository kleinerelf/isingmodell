module rand_mod
  use precession_mod, only: dp
  use iso_fortran_env, only: int64
  implicit none

  private

  type rand_t
     private
     logical :: init = .FALSE.
   contains
     procedure :: rand_real
     procedure :: rand_array
     procedure :: rand_2d_array
     generic, public :: rand => rand_real, rand_array, rand_2d_array
  end type rand_t

  interface rand_t
     module procedure construct
  end interface rand_t

  interface getpid
     function getpid () bind (C, name="getpid")
       ! mimic C typedef: rename C_INT => pid_t
       use iso_c_binding, only: pid_t => C_INT
       implicit none

       integer(kind=pid_t) :: getpid
     end function getpid
  end interface getpid

  public :: rand_t
contains
  type(rand_t) function construct()
    implicit none
    integer, allocatable :: seed(:)
    integer :: i, n, un, istat, dt(8), pid
    integer(int64) :: t

    call random_seed(size = n)
    allocate(seed(n))
    ! First try if the OS provides a random number generator
    open(newunit=un, file="/dev/urandom", access="stream", &
         form="unformatted", action="read", status="old", iostat=istat)
    if (istat == 0) then
       read(un) seed
       close(un)
    else
       ! Fallback to XOR:ing the current time and pid. The PID is
       ! useful in case one launches multiple instances of the same
       ! program in parallel.
       call system_clock(t)
       if (t == 0) then
          call date_and_time(values=dt)
          t = (dt(1) - 1970) * 365_int64 * 24 * 60 * 60 * 1000 &
               + dt(2) * 31_int64 * 24 * 60 * 60 * 1000 &
               + dt(3) * 24_int64 * 60 * 60 * 1000 &
               + dt(5) * 60 * 60 * 1000 &
               + dt(6) * 60 * 1000 + dt(7) * 1000 &
               + dt(8)
       end if
       pid = getpid()
       t = ieor(t, int(pid, kind(t)))
       do i = 1, n
          seed(i) = lcg(t)
       end do
    end if
    call random_seed(put=seed)

    construct%init = .TRUE.
  end function construct

  ! This simple PRNG might not be good enough for real work, but is
  ! sufficient for seeding a better PRNG.
  integer function lcg(s)
    integer(int64) :: s
    if (s == 0) then
       s = 104729
    else
       s = mod(s, 4294967296_int64)
    end if
    s = mod(s * 279470273_int64, 4294967291_int64)
    lcg = int(mod(s, int(huge(0), int64)), kind(0))
  end function lcg

  subroutine rand_real(self, random)
    class(rand_t) :: self
    real(dp), intent(out) :: random

    if (.NOT. self%init) then
       write(*, "(A)") "Random seed not set."
       stop
    end if
    call random_number(random)
  end subroutine rand_real

  subroutine rand_array(self, random)
    class(rand_t) :: self
    real(dp), dimension(:), intent(out) :: random

    if (.NOT. self%init) then
       write(*, "(A)") "Random seed not set."
       stop
    end if
    call random_number(random)
  end subroutine rand_array

  subroutine rand_2d_array(self, random)
    class(rand_t) :: self
    real(dp), dimension(:, :), intent(out) :: random

    if (.NOT. self%init) then
       write(*, "(A)") "Random seed not set."
       stop
    end if
    call random_number(random)
  end subroutine rand_2d_array
end module rand_mod
