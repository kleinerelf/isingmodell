module unit_test_mod
  use precession_mod, only: dp

  implicit none

  private

  real(dp), parameter :: tiny_10 = 1e-10, &
       & tiny_13 = 1e-3

  type :: unit_entry_test_t
     character(len=:), allocatable :: name
     character(len=:), allocatable :: description
     logical :: success = .false.

     type(unit_entry_test_t), pointer :: next => null()
  end type unit_entry_test_t

  type :: unit_test_t
     private
     type(unit_entry_test_t), pointer :: first => null()
     type(unit_entry_test_t), pointer :: last => null()

     integer :: n_success
     integer :: n_failure
   contains
     procedure, public :: add => unit_test_add
     procedure, public :: write => unit_test_write
     procedure, public :: final => unit_test_final
  end type unit_test_t

  abstract interface
     subroutine unit_test_p(success)
       logical, intent(out) :: success
     end subroutine unit_test_p
  end interface

  interface unit_test_t
     module procedure constructor
  end interface unit_test_t

  interface assert_equal
     module procedure assert_equal_real, assert_equal_integer
  end interface assert_equal

  interface vanishes
     module procedure vanishes_real, vanishes_complex
  end interface vanishes

  public :: assert, assert_equal, nearly_equal, test, tiny_10,&
       & tiny_13, unit_test_t, vanishes

contains

subroutine test(test_p, name, description, results)
  procedure(unit_test_p) :: test_p
  character(len=:), allocatable, intent(in) :: name
  character(len=:), allocatable, intent(in) :: description
  type(unit_test_t), intent(inout) :: results

  logical :: success

  call test_p(success)
  call results%add(name, description, success)
end subroutine test

type(unit_test_t) function constructor()
  associate (self => constructor)
    self%n_success = 0
    self%n_failure = 0
  end associate
end function constructor

subroutine unit_test_add(self, name, description, success)
  class(unit_test_t), intent(inout) :: self

  character(len=:), allocatable, intent(in) :: name
  character(len=:), allocatable, intent(in) :: description

  logical, intent(in) :: success

  type(unit_entry_test_t), pointer :: entry => null()

  allocate(entry)
  entry%name = name
  entry%description = description
  entry%success = success

  if( associated(self%first) ) then
     self%last%next => entry
  else
     self%first => entry
  end if

  self%last => entry

  if( success ) then
     self%n_success = self%n_success + 1
  else
     self%n_failure = self%n_failure + 1
  end if
end subroutine unit_test_add

subroutine unit_test_write(self)
  class(unit_test_t) :: self

  type(unit_entry_test_t), pointer :: entry => null ()
  print *, "**************************************************"
  print *, "* Test Summary"
  print *, "**************************************************"

  if( self%n_success > 0 ) then
     print *, "* Success:"
     entry => self%first
     do while( associated(entry) )
        if( entry%success ) then
           print *, "*     ", entry%name
           print *, "*     -", entry%description
        end if
        entry => entry%next
     end do
  end if

  if( self%n_failure > 0 ) then
     print *, "* Failure:"
     entry => self%first
     do while( associated(entry) )
        if( .not. entry%success ) then
           print *, "*    ", entry%name
           print *, "*    -", entry%description
        end if
        entry => entry%next
     end do
  end if

  print *, "* Number of tests:   ", self%n_failure + self%n_success
  print *, "* Number of success: ", self%n_success
  print *, "* Number of failure: ", self%n_failure

  print *, "**************************************************"
  print *, "* End: Test Summary"
  print *, "**************************************************"
end subroutine unit_test_write

subroutine unit_test_final(self)
  class(unit_test_t), intent(inout) :: self

  type(unit_entry_test_t), pointer :: result => null()

  do while( associated(self%first) )
     result => self%first
     self%first => result%next
     deallocate(result)
  end do
  self%last => null()
  self%n_success = 0
  self%n_failure = 0
end subroutine unit_test_final

subroutine assert(success, description)
  logical, intent(in) :: success
  character(len=:), allocatable, intent(in), optional :: description

  if (.not. success) then
     if (present(description)) then
        print *, "* FAIL: " // description
     else
        print *, "* FAIL: Assertion error"
     end if
  end if
end subroutine assert

subroutine assert_equal_real(lhs, rhs, description, &
     abs_smallness, rel_smallness)
  real(dp), intent(in) :: lhs, rhs
  character(len=:), allocatable, intent(in), optional :: description
  real(dp), intent(in), optional :: abs_smallness, rel_smallness

  logical :: success

  success = nearly_equal (lhs, rhs, abs_smallness, rel_smallness)
  if (.not. success) then
     if (present(description)) then
        print *, "* FAIL: " // description // ": ", lhs, " /= ", rhs
     else
        print *, "* FAIL: Assertion error: ", lhs, " /= ", rhs
     end if
  end if
end subroutine assert_equal_real

subroutine assert_equal_integer(lhs, rhs, description)
  integer, intent(in) :: lhs, rhs
  character(len=:), allocatable, intent(in), optional :: description

  logical :: success

  success = lhs == rhs

  if (.not. success) then
     if (present(description)) then
        print *, "* FAIL: " // description // ": ", lhs, " /= ", rhs
     else
        print *, "* FAIL: Assertion error: ", lhs, " /= ", rhs
     end if
  end if
end subroutine assert_equal_integer

elemental function ieee_is_nan(x) result (yorn)
  real(dp), intent(in) :: x

  logical :: yorn

  yorn = (x /= x)
end function ieee_is_nan

elemental function nearly_equal(a, b, abs_smallness, rel_smallness) result (success)
  real(dp), intent(in) :: a, b
  real(dp), intent(in), optional :: abs_smallness, rel_smallness

  logical :: success
  real(dp) :: abs_a, abs_b, diff, abs_small, rel_small

  abs_a = abs (a)
  abs_b = abs (b)
  diff = abs (a - b)

  ! shortcut, handles infinities and nans
  if (a == b) then
     success = .true.
     return
  else if( ieee_is_nan(a) .or. ieee_is_nan(b) .or. ieee_is_nan(diff) ) then
     success = .false.
     return
  end if

  abs_small = 1e-13; if( present(abs_smallness) ) abs_small = abs_smallness
  rel_small = 1e-10; if( present(rel_smallness) ) rel_small = rel_smallness
  if( abs_a < abs_small .and. abs_b < abs_small ) then
     success = diff < abs_small
  else
     success= diff / max(abs_a, abs_b) < rel_small
  end if
end function nearly_equal

elemental function vanishes_real (x, abs_smallness, rel_smallness) result (success)
  real(dp), intent(in) :: x
  real(dp), intent(in), optional :: abs_smallness, rel_smallness

  logical :: success

  success = nearly_equal(x, 0._dp, abs_smallness, rel_smallness)
end function vanishes_real

elemental function vanishes_complex (x, abs_smallness, rel_smallness) result (success)
  complex(dp), intent(in) :: x
  real(dp), intent(in), optional :: abs_smallness, rel_smallness

  logical :: success

  success = vanishes_real(abs(x), abs_smallness, rel_smallness)
end function vanishes_complex
end module unit_test_mod
