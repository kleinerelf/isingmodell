#+TITLE: Ising-Modell 
#+DATE: <2015-12-23 Wed>
#+AUTHOR: Simon Braß
#+EMAIL: brass@physik.uni-siegen.de
#+CREATOR: Emacs 25.1.50.1 (Org mode 8.2.10)
#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE: en
#+OPTIONS: toc:4     
#+BABEL: :session *fortran*
-----

* Introduction

** Zu deutsch:

Ich denke, die erste Frage die sich jemand stellen würde ist: Warum gibt es ein so auf wendiges Programm für ein so einfaches Problem?
Die Antwort(en) dazu:
1. Fortran 
2. Modern Fortran
3. Modern Fortran im Standard 2008
Die etwas ausführlichere Antwort lautet natürlich etwas anders.
Mit diesem Projekt wollte ich eine kurze und doch recht pausible Einführung geben, welche sich auf dem Schwerpunkt von Fortran stützt, nämlich der wissentschaftlichen Rechungen und Simulationen.
Darüber hinaus möchte ich das +moderne+ Fortran 2008 vorstellen und seine wunderbaren Einsatzmöglichkeiten aufzeigen, die dank der immer besseren Ünterstützung Compiler-seitig endlich genutzt werden können.

* Ising model

** Abstract interface model
  :PROPERTIES:
  :date:     <2015-12-23 Wed>
  :updated:  <2015-12-23 Wed>
  :END:

File: src/modell/modell.f08

#+INCLUDE: "modell/modell.org"

** Generic implementation of the abstract model
  :PROPERTIES:
  :date:     <2015-12-23 Wed>
  :updated:  <2015-12-23 Wed>
  :END:

File: src/modell/modell_implementation.f08

#+INCLUDE: "modell/modell_implementation.org"


* Utils

File: src/utils/statistics.f08

#+INCLUDE: "utils/statistics.org"

* Unittest
  :PROPERTIES:
  :date:     <2015-12-23 Wed>
  :updated:  <2015-12-23 Wed>
  :END:

File: src/utils/unit\textunderscore{}test.f08

#+INCLUDE: "utils/unit_test.org"

** statistics
  :PROPERTIES:
  :date:     <2015-12-23 Wed>
  :updated:  <2015-12-23 Wed>
  :END:

File: src/test/statistics\textunderscore{}TEST.f08

#+INCLUDE: "test/statistics_TEST.org"
