program main
  use precession_mod
  use statistics_mod
  use M_kracken
  use modell_mod
  use modell_impl_mod

  use omp_lib

  implicit none

  ! Kommandozeilen Argumente
  character(len=255) :: filename
  integer :: sys_len, max_iter, burnin, sweep, iflen, ier
  logical :: wolff_mode
  real(dp) :: coupling, mag_field, temp

  ! Modell
  character(len=255) :: modell_name
  type(modell_impl_t) :: modell

  ! Messgrößen
  type(mean_var_t) :: mag, energy

  integer :: j, k, file_unit, open_status, close_status
  real(dp) :: start_cpu, end_cpu

  ! **************************************************
  call kracken("cmd", "-o out.dat -wolff .F. -c 1.0 -m 0.0 -n 100 -max 10000 &
       &-burnin 1000 -sweep 200 -temp 1.0 -h .F. -help .F. -v        .F. -version .F.")

  call retrev("cmd_o", filename, iflen, ier)

  sys_len = iget("cmd_n")
  max_iter = iget("cmd_max")
  burnin = iget("cmd_burnin")
  sweep = iget("cmd_sweep")

  wolff_mode = lget("cmd_wolff")

  coupling = rget("cmd_c")
  mag_field = rget("cmd_m")
  temp = rget("cmd_temp")

  if (lget("cmd_h") .OR. lget("cmd_help")) then
     ! Help Message
     write(*, "(A)") "main debug"
     write(*, "(A)") " [-o STRING]             # defualt_value = out.dat"
     write(*, "(A)") " [-wolff]                # default: Metropolis Algorithmus"
     write(*, "(A)") " [-c REAL]               # default value = 1.0"
     write(*, "(A)") " [-m REAL]               # default value = 0.0"
     write(*, "(A)") " [-n INTEGER]            # default value = 100"
     write(*, "(A)") " [-max INTEGER]          # default_value = 10000"
     write(*, "(A)") " [-burnin INTEGER]       # default_value = 1000"
     write(*, "(A)") " [-sweep INTEGER]        # default_value = 200"
     write(*, "(A)") " [-temp REAL]            # default_value = 1.0"
     write(*, "(A)") " [-h|-help|--help]|      # help flag=.F."
     write(*, "(A)") " [-v|-version|--version] # version flag=.F."
     write(*, "(A)") ""
     stop
  end if

  if (lget("cmd_v") .OR. lget("cmd_version")) then
     ! Version Message
     write(*, "(A)") "LatticeGas. Simon Brass und Markus Kurbel. v0.4"
     stop
  end if

  write(*, "(A)") "#**************************************************"
  if (wolff_mode) then
     write(*, "(A)") "# Wolff-Algorithmus (DEBUG)"
  else
     write(*, "(A)") "# Metropolis-Algorithmus (DEBUG)"
  end if
  write(*, "(A)") "# Speichern in: "//filename(:iflen)
  write(*, "(A, F12.6)") "# J: ", coupling
  write(*, "(A, F12.6)") "# H: ", mag_field
  write(*, "(A, I0)") "# Systemgröße: ", sys_len
  write(*, "(A, I0)") "# Max. Iteration: ", max_iter
  write(*, "(A, I0)") "# Einlaufschritte: ", burnin
  write(*, "(A, I0)") "# MC-Sweep: ", sweep
  write(*, "(A, F12.6)") "# kT: ", temp
  write(*, "(A)") "#************************************************&
       &**"
  ! **************************************************

  file_unit = 10
  open(unit=file_unit, file=filename(:iflen), status='replace',&
       & iostat=open_status, action='write')
  if (open_status /= 0) then
     write(*, "(A, I0)") 'Could not open '//filename(:iflen)//' for&
          & writing. unit = ', file_unit
     stop
  endif

  write(file_unit, "(A)") "#**************************************************"
  if (wolff_mode) then
     write(file_unit, "(A)") "# Wolff-Algorithmus (DEBUG)"
  else
     write(file_unit, "(A)") "# Metropolis-Algorithmus (DEBUG)"
  end if
  write(file_unit, "(A)") "# Speichern in: "//filename(:iflen)
  write(file_unit, "(A, F12.6)") "# J: ", coupling
  write(file_unit, "(A, F12.6)") "# H: ", mag_field
  write(file_unit, "(A, I0)") "# Systemgröße: ", sys_len
  write(file_unit, "(A, I0)") "# Max. Iteration: ", max_iter
  write(file_unit, "(A, I0)") "# Einlaufschritte: ", burnin
  write(file_unit, "(A, I0)") "# MC-Sweep: ", sweep
  write(*, "(A, F12.6)") "# kT: ", temp
  write(file_unit, "(A)") "#************************************************&
       &**"
  write(file_unit, "(A)") "# E                 M"

  call cpu_time(start_cpu)

  if (wolff_mode) then
     modell_name = "wolff"
  else
     modell_name = "metropolis"
  end if

  modell = modell_impl_t(trim(modell_name), sys_len, mag_field, coupling)

  ! Einlaufzeit
  do j = 1, burnin
     call modell%flip_spin(temp)
     write(file_unit, "(F12.6, F12.6)") abs(modell%get_energy()), abs(modell%get_magnetization())
  end do

  k = 0

  ! Messung
  do j = burnin, max_iter
     call modell%flip_spin(temp)

     k = k + 1

     if (k .GT. sweep) then
        call energy%update(abs(modell%get_energy()))
        call mag%update(abs(modell%get_magnetization()))
        k = 0
     end if
  end do

  ! Ausgabe
  write(file_unit, "('#', F12.6, ' ', F32.16, ' ', F32.16, ' ', F32.16 &
       &, '', F32.16)") temp, energy%get_mean(),&
       & energy%get_var(), mag%get_mean(), mag%get_var()
  call modell%clean()

  call cpu_time(end_cpu)
  write(*, "(A, F20.8)") "# CPU: ", end_cpu - start_cpu

  close(file_unit, iostat=close_status)
  if ( close_status /= 0 ) then
     write(*, "(A, I0)") 'Error: Attempt to close a file that is not&
          & open. unit = ', file_unit
     stop
  endif
end program main
