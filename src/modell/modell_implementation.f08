module modell_impl_mod
  use precession_mod, only: dp
  use message_mod
  use modell_mod
  use wolff_mod
  use metropolis_mod

  implicit none

  private

  type modell_impl_t
     class(modell_t), allocatable :: impl
   contains
     procedure, public :: clean => clean
     procedure, public :: flip_spin => flip_spin
     procedure, public :: get_energy => get_energy
     procedure, public :: get_correlation => get_correlation
     procedure, public :: get_magnetization => get_magnetization
     procedure, public :: print_grid => print_grid
  end type modell_impl_t

  interface modell_impl_t
     module procedure constructor
  end interface modell_impl_t

  public :: modell_impl_t

contains

  type(modell_impl_t) function constructor(name, n, mag_field, coupling)
    character(*), intent(in) :: name
    integer, intent(in) :: n
    real(dp), intent(in) :: mag_field, coupling

    select case (name)
    case("wolff")
       allocate(constructor%impl, source=wolff_t(n, mag_field, coupling))
    case("metropolis")
       allocate(constructor%impl, source=metropolis_t(n, mag_field, coupling))
    case default
       call error_handler%print("Passendes Modell nicht gefunden.", critical)
    end select
  end function constructor

  subroutine clean(self)
    class(modell_impl_t) :: self

    ! Delete the hard way
    deallocate(self%impl)
  end subroutine clean

  subroutine flip_spin(self, kT)
    class(modell_impl_t), intent(in) :: self
    real(dp), intent(in) :: kT

    if (allocated(self%impl) .eqv. .false.) then
       call error_handler%print("modell_impl_t%flip_spin:&
            & implementation not allocated.", critical)
    end if

    select type (modell => self%impl)
    class default
       call modell%flip_spin(kT)
    end select
  end subroutine flip_spin

  real(dp) function get_energy(self)
    class(modell_impl_t), intent(in) :: self

    if (allocated(self%impl) .eqv. .false.) then
       call error_handler%print("modell_impl_t%get_energy:&
            & implementation not allocated.", critical)
    end if

    select type (modell => self%impl)
    class default
       get_energy = modell%get_energy()
    end select
  end function get_energy

  real(dp) function get_correlation(self, i, j)
    class(modell_impl_t), intent(in) :: self
    integer, intent(in) :: i, j

    if (allocated(self%impl) .eqv. .false.) then
       call error_handler%print("modell_impl_t%get_correlation:&
            & implementation not allocated.", critical)
    end if

    select type (modell => self%impl)
    class default
       get_correlation = modell%get_correlation(i, j)
    end select
  end function get_correlation

  real(dp) function get_magnetization(self)
    class(modell_impl_t), intent(in) :: self

    if (allocated(self%impl) .eqv. .false.) then
       call error_handler%print("modell_impl_t%get_magnetization:&
            & implementation not allocated.", critical)
    end if

    select type(modell => self%impl)
    class default
       get_magnetization = modell%get_magnetization()
    end select
  end function get_magnetization

  subroutine print_grid(self)
    class(modell_impl_t), intent(in) :: self

    if (allocated(self%impl) .eqv. .false.) then
       call error_handler%print("modell_impl_t%print_grid:&
            & implementation not allocated.", critical)
    end if

    select type (modell => self)
    class default
       call modell%print_grid()
    end select
  end subroutine print_grid
end module modell_impl_mod
