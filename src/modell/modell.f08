module modell_mod
  use precession_mod, only: dp
  use rand_mod
  use statistics_mod
  implicit none

  private

  type, abstract :: modell_t
     integer :: sys_len

     real(dp) :: energy, &
          mag_field = 0._dp, &
          couple_const = 1._dp

     type(rand_t) :: rand_gen
   contains
     procedure(clean), deferred, pass, public :: clean
     procedure(get_energy), deferred, pass, public :: get_energy
     procedure(get_magnetization), deferred, pass, public :: get_magnetization
     procedure(get_correlation), deferred, pass, public :: get_correlation
     procedure(flip_spin), deferred, pass, public :: flip_spin
     procedure(print_grid), deferred, pass, public :: print_grid
  end type modell_t

  abstract interface
     subroutine clean(self)
       import modell_t
       class(modell_t) :: self
     end subroutine clean

     real(dp) function get_energy(self)
       import dp, modell_t
       class(modell_t) :: self
     end function get_energy

     real(dp) function get_magnetization(self)
       import dp, modell_t
       class(modell_t) :: self
     end function get_magnetization

     real(dp) function get_correlation(self, i, j)
       import dp, modell_t
       class(modell_t) :: self
       integer, intent(in) :: i, j
     end function get_correlation

     subroutine flip_spin(self, kT)
       import dp, modell_t
       class(modell_t) :: self
       real(dp), intent(in) :: kT
     end subroutine flip_spin

     subroutine print_grid (self)
       import modell_t
       class(modell_t) :: self
     end subroutine print_grid
  end interface

  public :: modell_t
end module modell_mod
