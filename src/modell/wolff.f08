! http://arxiv.org/pdf/cond-mat/0311623.pdf
module wolff_mod
  use precession_mod, only: dp
  ! use list ! redundant
  use modell_mod
  use rand_mod
  use statistics_mod
  implicit none

  private

  type, extends(modell_t) :: wolff_t
     private
     integer, dimension(:, :), allocatable :: grid
   contains
     procedure, public :: clean => clean
     procedure, public :: diff_grid => diff_grid ! Debug
     procedure, public :: flip_spin => flip_spin
     procedure, public :: get_energy => get_energy
     procedure, public :: get_correlation => get_correlation
     procedure, public :: get_grid => get_grid ! Debug
     procedure, public :: get_magnetization => get_magnetization
     procedure, public :: print_grid => print_grid

     procedure, private :: calc_energy
     procedure, private :: cyclic_coord
     procedure, private :: update_cluster
     procedure, private :: update_cluster_worker
  end type wolff_t

  interface wolff_t
     module procedure constructor
  end interface wolff_t

  public :: wolff_t
contains
  type(wolff_t) function constructor(n, mag_field, coupling)
    integer, intent(in) :: n
    real(dp), intent(in) :: mag_field, coupling

    real(dp), dimension(:,:), allocatable :: randgrid

    constructor%sys_len = n
    constructor%mag_field = mag_field
    constructor%couple_const = coupling

    constructor%rand_gen = rand_t()

    allocate(constructor%grid(constructor%sys_len, constructor&
         &%sys_len))

    allocate(randgrid(constructor%sys_len, constructor%sys_len))

    call constructor%rand_gen%rand(randgrid)

    constructor%grid = merge(1, -1, randgrid<=0.5)

    constructor%energy = constructor%calc_energy()
  end function constructor

  subroutine clean(self)
    class(wolff_t) :: self

    deallocate(self%grid)
  end subroutine clean

  subroutine diff_grid(self, grid_co)
    class(wolff_t), intent(in) :: self
    integer, dimension(self%sys_len, self%sys_len), intent(in) :: grid_co

    integer :: i, j
    do i = 1, self%sys_len
       do j = 1, self%sys_len
          if (grid_co(i, j) /= self%grid(i, j)) then
             write( *, "(A)", advance="no") " X "
          else
             write( *, "(A)", advance="no") " 0 "
          end if
       end do
       print *, ""
    end do
  end subroutine diff_grid

  subroutine print_grid (self)
    class(wolff_t) :: self
    integer :: i

    do i = 1, self%sys_len
       print *, self%grid(i, :)
    end do
  end subroutine print_grid

  subroutine flip_spin(self, kT)
    class(wolff_t) :: self
    real(dp), intent(in) :: kT

    real(dp) :: prob

    prob =  1.0_dp - exp(-2.0_dp * self%couple_const / kT)
    call self%update_cluster(prob)
  end subroutine flip_spin

  real(dp) function get_energy(self)
    class(wolff_t) :: self

    ! BUGFIX: Erst neue Energie berechnen und dann ausgeben...
    self%energy = self%calc_energy()

    ! Normaliserung pro Spin
    get_energy = self%energy
  end function get_energy

  real(dp) function get_magnetization(self)
    class(wolff_t) :: self

    ! Normalisierung pro Spin
    get_magnetization = sum(self%grid)
  end function get_magnetization

  real(dp) function get_correlation(self, i, j)
    class(wolff_t) :: self
    integer, intent(in) :: i, j

    type(mean_var_t) :: spin_spin
    integer :: d, m, n, up, down, left, right

    if (i .EQ. j) then
       get_correlation = 0._dp
    else
       d = abs(i - j)
       do m = 1, self%sys_len
          do n = 1, self%sys_len
             if (.NOT. (abs(m - n) .EQ. d)) then
                cycle
             end if

             call self%cyclic_coord(m, n, up, down, left, right)

             call spin_spin%update(1.0_dp *  self%grid(m, n) * (self%grid(m, left) +&
                  & self%grid(m, right) + self%grid(up, n) + self&
                  &%grid(down, n)))
          end do
       end do

       get_correlation = 0.5**2 * spin_spin%get_mean() / 2.0 + self&
            &%get_magnetization()
    end if
  end function get_correlation

  function get_grid(self)
    class(wolff_t), intent(in) :: self
    integer, dimension(self%sys_len, self%sys_len) :: get_grid

    get_grid = self%grid
  end function get_grid

  real(dp) function calc_energy(self)
    class(wolff_t) :: self

    integer :: m, n, up, down, left, right

    calc_energy = 0._dp
    do n = 1, self%sys_len
       do m = 1, self%sys_len
          call self%cyclic_coord(m, n, up, down, left, right)

          calc_energy = calc_energy - self&
               &%grid(m, n) * (self%grid(m, left) + self%grid(up, n))
       end do
    end do
    calc_energy = - 0.5_dp * self%couple_const * calc_energy - self%mag_field * self%get_magnetization()
  end function calc_energy

  subroutine cyclic_coord (self, m, n, up, down, left, right)
    class(wolff_t) :: self

    integer, intent(in) :: m, n
    integer, intent(out) :: up, down, left, right

    !zyklische Randbedingungen implementieren
    up = m-1
    down = m+1
    left = n-1
    right = n+1

    if (m == 1) up = self%sys_len
    if (n == 1) left = self%sys_len
    if (m == self%sys_len) down = 1
    if (n == self%sys_len) right = 1
  end subroutine cyclic_coord

  subroutine update_cluster(self, prob)
    class(wolff_t), intent(inout) :: self
    real(dp), intent(in) ::prob

    integer :: state
    real(dp), dimension(2) :: rand_coord
    call self%rand_gen%rand(rand_coord)
    rand_coord = (self%sys_len * rand_coord + 1.0)

    associate (m => int(rand_coord(1)), n => int(rand_coord(2)))
      state = self%grid(m, n)
      call self%update_cluster_worker(m, n, state, prob)
    end associate
  end subroutine update_cluster

  recursive subroutine update_cluster_worker(self, m, n, state, prob)
    class(wolff_t), intent(inout) :: self
    integer, intent(in) :: m, n, state
    real(dp), intent(in) :: prob

    integer :: up, left, down, right
    real(dp) :: rand_prob

    self%grid(m, n) = -self%grid(m, n)

    call self%cyclic_coord(m, n, up, down, left, right)

    if( state == self%grid(up, n) ) then
       call self%rand_gen%rand(rand_prob)
       if( rand_prob < prob ) then
          call self%update_cluster_worker(up, n, state, prob)
       end if
    end if

    if( state == self%grid(down, n) ) then
       call self%rand_gen%rand(rand_prob)
       if( rand_prob < prob ) then
          call self%update_cluster_worker(down, n, state, prob)
       end if
    end if

    if( state == self%grid(m, left) ) then
       call self%rand_gen%rand(rand_prob)
       if( rand_prob < prob ) then
         call self%update_cluster_worker(m, left, state, prob)
       end if
    end if

    if( state == self%grid(m, right) ) then
       call self%rand_gen%rand(rand_prob)
       if( rand_prob < prob ) then
          call self%update_cluster_worker(m, right, state, prob)
       end if
    end if
  end subroutine  update_cluster_worker
end module wolff_mod
