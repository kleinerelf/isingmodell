module metropolis_mod
  use precession_mod, only: dp
  use modell_mod
  use rand_mod
  use statistics_mod
  implicit none

  private

  type, extends(modell_t) :: metropolis_t
     private
     integer, dimension(:, :), allocatable :: grid
   contains
     procedure, public :: clean => clean
     procedure, public :: flip_spin => flip_spin
     procedure, public :: get_energy => get_energy
     procedure, public :: get_correlation => get_correlation
     procedure, public :: get_magnetization => get_magnetization
     procedure, public :: print_grid => print_grid

     procedure, private :: calc_energy
     procedure, private :: cyclic_coord
  end type metropolis_t

  interface metropolis_t
     module procedure constructor
  end interface metropolis_t

  public :: metropolis_t
contains
  type(metropolis_t) function constructor(n, mag_field, coupling)
    integer, intent(in) :: n
    real(dp), intent(in) :: mag_field, coupling

    real(dp), dimension(:,:), allocatable :: randgrid

    constructor%sys_len = n
    constructor%mag_field = mag_field
    constructor%couple_const = coupling

    constructor%rand_gen = rand_t()

    allocate(constructor%grid(constructor%sys_len, constructor&
         &%sys_len))
    allocate(randgrid(constructor%sys_len, constructor%sys_len))

    call constructor%rand_gen%rand(randgrid)

    constructor%grid = merge(1, -1, randgrid<=0.5)

    constructor%energy = constructor%calc_energy()
  end function constructor

  subroutine clean(self)
    class(metropolis_t) :: self

    deallocate(self%grid)
  end subroutine clean

  real(dp) function get_energy(self)
    class(metropolis_t) :: self

    self%energy = self%calc_energy()
    get_energy = self%energy
  end function get_energy

  real(dp) function get_magnetization(self)
    class(metropolis_t) :: self

    ! Normalisierung pro Spin
    get_magnetization = sum(self%grid)
  end function get_magnetization

  real(dp) function get_correlation(self, i, j)
    class(metropolis_t) :: self
    integer, intent(in) :: i, j

    type(mean_var_t) :: spin_spin
    integer :: d, m, n, up, down, left, right

    if (i .EQ. j) then
       get_correlation = 0._dp
    else
       d = abs(i - j)
       do m = 1, self%sys_len
          do n = 1, self%sys_len
             if (.NOT. (abs(m - n) .EQ. d)) then
                cycle
             end if

             call cyclic_coord (self, m, n, up, down, left, right)

             call spin_spin%update(1.0_dp *  self%grid(m, n) * (self%grid(m, left) +&
                  & self%grid(m, right) + self%grid(up, n) + self&
                  &%grid(down, n)))
          end do
       end do

       get_correlation = 0.5**2 * spin_spin%get_mean() / 2.0 + self&
            &%get_magnetization()
    end if
  end function get_correlation

  real(dp) function calc_energy(self)
    class(metropolis_t) :: self

    integer :: m, n, up, down, left, right

    calc_energy = 0._dp
    do m = 1, self%sys_len
       do n = 1, self%sys_len
          call cyclic_coord (self, m, n, up, down, left, right)

          calc_energy = calc_energy - self%couple_const * self&
               &%grid(m, n) * (self%grid(m, left) + self%grid(up, n))
       end do
    end do

    calc_energy = 0.5_dp * (calc_energy - self%mag_field * self%get_magnetization())
  end function calc_energy

  subroutine flip_spin(self, kT)
    class(metropolis_t) :: self
    real(dp), intent(in) :: kT

    integer ::  m, n, up, down, left, right
    real(dp), dimension(2) :: randkoord
    real(dp) :: rand

    real(dp) :: deltaE

    call self%rand_gen%rand(randkoord)
    randkoord = self%sys_len * randkoord
    m = int(randkoord(1)) + 1
    n = int(randkoord(2)) + 1

    call cyclic_coord (self, m, n, up, down, left, right)

    deltaE = 2.0_dp * ( self%couple_const * self&
         &%grid(m, n)*(self%grid(m, left) + self%grid(m, right) +&
         & self%grid(up, n) + self%grid(down, n)) + self%mag_field &
         &* self%grid(m, n))

    call self%rand_gen%rand(rand)
    if ( deltaE <= 0.0_dp ) then
       self%grid(m, n) = -1 * self%grid(m, n) ! Flip Spin
    else
       if ( rand < min(1.0_dp, exp(-( deltaE / kT)))) then
          self%grid(m, n) = -1 * self%grid(m, n) ! Flip Spin
       end if
    end if
  end subroutine flip_spin

  subroutine cyclic_coord (self, m, n, up, down, left, right)
    class(metropolis_t) :: self

    integer, intent(in) :: m, n
    integer, intent(out) :: up, down, left, right

    !zyklische Randbedingungen implementieren
    up = m-1
    down = m+1
    left = n-1
    right = n+1

    if (m == 1) up = self%sys_len
    if (n == 1) left = self%sys_len
    if (m == self%sys_len) down = 1
    if (n == self%sys_len) right = 1
  end subroutine cyclic_coord

  subroutine print_grid (self)
    class(metropolis_t) :: self
    integer :: i

    do i = 1, self%sys_len
       print *, self%grid(i, :)
    end do
  end subroutine print_grid
end module metropolis_mod
