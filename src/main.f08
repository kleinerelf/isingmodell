program main
  use precession_mod
  use statistics_mod
  use M_kracken
  use message_mod
  use modell_mod
  use modell_impl_mod

  use omp_lib

  implicit none

  type :: cmd_arg_t
     character(len=:), allocatable :: f_output
     integer :: sys_len, max, burn, sweep, threads
     logical :: wolff_mode, debug_mode
     real(dp) :: coupling, mag_field, start, end, step
  end type cmd_arg_t

  type(cmd_arg_t) :: cmd_arg
  character(:), allocatable :: cmd_arg_fmt
  character(500) :: cmd_arg_opt

  character(200) :: message_str

  ! Modell
  character(:), allocatable :: modell_name
  type(modell_impl_t), dimension(:), allocatable :: modell

  ! Messgrößen
  type(mean_var_t), dimension(:), allocatable :: mag, energy
  real(dp), dimension(:), allocatable :: kT

  integer :: i, j, k, n_steps, file_unit, open_status, close_status
  real(dp) :: start_cpu, end_cpu

  ! **************************************************
  ! Debug-Mode
  ! **************************************************
  call error_handler%set_report_level(warning)

  ! **************************************************
  ! Commandline
  ! **************************************************
  cmd_arg = cmd_arg_t( &
       & f_output = "out.dat", &
       & sys_len = 100, &
       & max = 10000, &
       & burn = 1000, &
       & sweep = 200, &
       & threads = 3, &
       & wolff_mode = .false., &
       & coupling = 1.0_dp, &
       & mag_field = 0.0_dp, &
       & start = 0.25_dp, &
       & end = 5.0_dp, &
       & step = 0.25, &
       & debug_mode = .false.)

  cmd_arg_fmt = "('-o ', A, ' -n ', I0, ' -max ', I0, ' -burnin ', I0, &
       & ' -sweep ', I0, ' -threads ', I0, ' -wolff ', L1, ' -c ', F7.5, &
       & ' -m ', F7.5, ' -start ', F7.5, ' -end ', F7.5, ' -step ', F7.5, &
       & ' -h F -help F -v F -version F', ' -debug ', L1)"

  write(cmd_arg_opt, cmd_arg_fmt) cmd_arg%f_output, cmd_arg%sys_len, cmd_arg%max, cmd_arg%burn, &
       & cmd_arg%sweep, cmd_arg%threads, cmd_arg%wolff_mode, cmd_arg%coupling, cmd_arg%mag_field, &
       & cmd_arg%start, cmd_arg%end, cmd_arg%step, cmd_arg%debug_mode

  call cmd_arg_get(cmd_arg, cmd_arg_opt)

  ! **************************************************
  if (cmd_arg%debug_mode) then
     call error_handler%set_report_level(debug)
  end if
  ! **************************************************
  if (lget("cmd_h") .OR. lget("cmd_help") .OR. lget("cmd_v") .OR. lget("cmd_version")) then
     call cmd_arg_help(cmd_arg_opt)
     stop
  end if

  call cmd_arg_print(cmd_arg)
  ! **************************************************
  file_unit = 10
  open(unit=file_unit, file=cmd_arg%f_output, status='replace',&
       & iostat=open_status, action='write')
  if (open_status /= 0) then
     write(message_str, "(A, I0)") 'Could not open '//cmd_arg%f_output//' for&
          & writing. unit = ', file_unit
     call error_handler%print(trim(message_str), critical)
  endif

  call cmd_arg_print(cmd_arg, file_unit)
  ! **************************************************

  write(file_unit, "(A)") "# kT     <E>     Var(E)     <M>     Var(M) / (NkT)"

  call omp_set_num_threads(cmd_arg%threads)

  n_steps = int((cmd_arg%end - cmd_arg%start) / cmd_arg%step) + 1
  if (cmd_arg%wolff_mode) then
     modell_name = "wolff"
  else
     modell_name = "metropolis"
  end if

  allocate(modell(n_steps))
  allocate(energy(n_steps), mag(n_steps), kT(n_steps))

  do i = 1, n_steps
     kT(i) = cmd_arg%start + (i - 1) * cmd_arg%step
  end do

  call cpu_time(start_cpu)

  !$OMP PARALLEL DO SCHEDULE(DYNAMIC, 8)&
  !$OMP PRIVATE (j, k)
  do i = 1, n_steps
     write(*, "(A, F12.6)") "Messung für kT: ", kT(i)
     modell(i) = modell_impl_t(trim(modell_name), cmd_arg%sys_len, cmd_arg%mag_field, cmd_arg%coupling)

     ! Einlaufzeit
     write(message_str, "(A, F12.6)") "Burn-in for kT=", kT(i)
     call error_handler%print(trim(message_str), debug)
     do j = 1, cmd_arg%burn
        call modell(i)%flip_spin(kT(i))
     end do

     k = 0

     ! Messung
     write(message_str, "(A, F12.6)") "Start measurement for kT=", kT(i)
     call error_handler%print(trim(message_str), debug)
     do j = cmd_arg%burn, cmd_arg%max
        call modell(i)%flip_spin(kT(i))

        k = k + 1

        if (k .GT. cmd_arg%sweep) then
           write(message_str, "(A, F12.6)") "Sweep complete for kT=", kT(i)
           call error_handler%print(trim(message_str), debug)
           call energy(i)%update(abs(modell(i)%get_energy()))
           call mag(i)%update(abs(modell(i)%get_magnetization()))
           k = 0
        end if
     end do

     ! Ausgabe
     write(file_unit, "(F12.6, ' ', F32.16, ' ', F32.16, ' ', F32.16 &
          &, '', F32.16)") kT(i), energy(i) %get_mean(),&
          & energy(i) %get_var(), mag(i)%get_mean(), mag(i)%get_var()
     call modell(i)%clean()
  end do
  !$OMP END PARALLEL DO

  call cpu_time(end_cpu)
  write(*, "(A, F20.8)") "# CPU: ", end_cpu - start_cpu

  close(file_unit, iostat=close_status)
  if ( close_status /= 0 ) then
     write(message_str, "(A, I0)") 'Error: Attempt to close a file that is not&
          & open. unit = ', file_unit
     call error_handler%print(message_str, critical)
     stop
  endif
contains
  subroutine cmd_arg_get(cmd_arg, cmd_arg_opt)
    type(cmd_arg_t), intent(inout) :: cmd_arg
    character(*), intent(in) :: cmd_arg_opt

    character(len=500) :: f_output
    integer :: iflen, ier

    call kracken("cmd", trim(cmd_arg_opt))

    ! Workaround for kracken
    call retrev("cmd_o", f_output, iflen, ier)
    cmd_arg%f_output = f_output(:iflen)

    write(message_str, "(A, I0)") "Length of cmd_arg%f_output: ", iflen
    call error_handler%print(trim(message_str), debug)
    call error_handler%print(cmd_arg%f_output, debug)

    cmd_arg%sys_len = iget("cmd_n")
    cmd_arg%max = iget("cmd_max")
    cmd_arg%burn = iget("cmd_burnin")
    cmd_arg%sweep = iget("cmd_sweep")
    cmd_arg%threads = iget("cmd_threads")

    cmd_arg%wolff_mode = lget("cmd_wolff")
    cmd_arg%debug_mode = lget("cmd_debug")

    cmd_arg%coupling = rget("cmd_c")
    cmd_arg%mag_field = rget("cmd_m")
    cmd_arg%start = rget("cmd_start")
    cmd_arg%end = rget("cmd_end")
    cmd_arg%step = rget("cmd_step")
  end subroutine cmd_arg_get

  subroutine cmd_arg_help(cmd_arg_opt)
    character(*), intent(in) :: cmd_arg_opt
    write(*, "(A)") "Ising-Modell. Implementierung mit Metropolis- und Wolff--Algorithmus in Fortran 2008."

    ! Help Message
    write(*, "(A)") "main"
    write(*, "(A)") " [-o STRING]"
    write(*, "(A)") " [-wolff]"
    write(*, "(A)") " [-debug]"
    write(*, "(A)") " [-c REAL]"
    write(*, "(A)") " [-m REAL]"
    write(*, "(A)") " [-n INTEGER]"
    write(*, "(A)") " [-max INTEGER]"
    write(*, "(A)") " [-burnin INTEGER]"
    write(*, "(A)") " [-sweep INTEGER]"
    write(*, "(A)") " [-start REAL]"
    write(*, "(A)") " [-end REAL]"
    write(*, "(A)") " [-step REAL]"
    write(*, "(A)") " [-threads INTEGER]"
    write(*, "(A)") " [-h|-help|--help]|"
    write(*, "(A)") " [-v|-version|--version]"
    write(*, "(A)") ""
    write(*, "(A)") "Default:"
    write(*, "(A)") trim(cmd_arg_opt)
  end subroutine cmd_arg_help

  subroutine cmd_arg_print(cmd_arg, file_unit)
    type(cmd_arg_t), intent(in) :: cmd_arg
    integer, intent(in), optional :: file_unit

    if (present(file_unit)) then
       write(file_unit, "(A)") "#**************************************************"
       if (cmd_arg%wolff_mode) then
          write(file_unit, "(A)") "# Wolff-Algorithmus"
       else
          write(file_unit, "(A)") "# Metropolis-Algorithmus"
       end if
       write(file_unit, "(A, L1)") "# Debug-Mode: ", cmd_arg%debug_mode
       write(file_unit, "(A)") "# Speichern in: "//cmd_arg%f_output
       write(file_unit, "(A, F12.6)") "# J: ", cmd_arg%coupling
       write(file_unit, "(A, F12.6)") "# H: ", cmd_arg%mag_field
       write(file_unit, "(A, I0)") "# Systemgröße: ", cmd_arg%sys_len
       write(file_unit, "(A, I0)") "# Max. Iteration: ", cmd_arg%max
       write(file_unit, "(A, I0)") "# Einlaufschritte: ", cmd_arg%burn
       write(file_unit, "(A, I0)") "# MC-Sweep: ", cmd_arg%sweep
       write(file_unit, "(A, F12.6, A, F12.6, A, F12.6)") "# kT in [", cmd_arg%start, ":",&
            & cmd_arg%end, "], Schritte: ", cmd_arg%step
       write(file_unit, "(A, I0)") "# Threads: ", cmd_arg%threads
       write(file_unit, "(A)") "#************************************************&
            &**"
    else
       write(*, "(A)") "#**************************************************"
       if (cmd_arg%wolff_mode) then
          write(*, "(A)") "# Wolff-Algorithmus"
       else
          write(*, "(A)") "# Metropolis-Algorithmus"
       end if
       write(*, "(A, L1)") "# Debug-Mode: ", cmd_arg%debug_mode
       write(*, "(A)") "# Speichern in: "//cmd_arg%f_output
       write(*, "(A, F12.6)") "# J: ", cmd_arg%coupling
       write(*, "(A, F12.6)") "# H: ", cmd_arg%mag_field
       write(*, "(A, I0)") "# Systemgröße: ", cmd_arg%sys_len
       write(*, "(A, I0)") "# Max. Iteration: ", cmd_arg%max
       write(*, "(A, I0)") "# Einlaufschritte: ", cmd_arg%burn
       write(*, "(A, I0)") "# MC-Sweep: ", cmd_arg%sweep
       write(*, "(A, F12.6, A, F12.6, A, F12.6)") "# kT in [", cmd_arg%start, ":",&
            & cmd_arg%end, "], Schritte: ", cmd_arg%step
       write(*, "(A, I0)") "# Threads: ", cmd_arg%threads
       write(*, "(A)") "#************************************************&
            &**"
    end if
  end subroutine cmd_arg_print
end program main
