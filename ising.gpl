set title "Spezifische Wärmekapazität"

set xlabel "kT"
set ylabel "Spezifische Wärmekapazität"

set xrange [0:5]

set terminal pngcairo size 1240,860
set output "specific_heat.png"

plot "ising50.dat" using 1:2 title "l=50", "ising100.dat" using 1:2 title "l=100",\
     "ising150.dat" using 1:2 title "l=150", "ising200.dat" using 1:2 title "l=200"

set title "Finite-Size-Scaling"

set xlabel "T"
set ylabel "<M>*L"

set terminal pngcairo size 1240,860
set output "finit_sizespecific_heat.png"

plot "ising50.dat" using 1:($3*50**(1/8)) title "l=50", "ising100.dat" using 1:($3*100**(1/8)) title "l=100",\
     "ising150.dat" using 1:($3*150**(1/8)) title "l=150", "ising200.dat" using 1:($3*200**(1/8)) title "l=200"

set title "Finite-Size-Scaling (Fehlerbalken)"

set xlabel "T"
set ylabel "<M>*L"

set terminal pngcairo size 1240,860
set output "finit_size_error.png"

plot "ising50.dat" using 1:($4*50**(1/8)):($5*50**(1/8)) title "l=50" with yerrorbar, "ising100.dat" using 1:($4*100**(1/8)):($5*100**(1/8)) title "l=100" with yerrorbar,\
     "ising150.dat" using 1:($4*150**(1/8)):($5*150**(1/8)) title "l=150" with yerrorbar, "ising200.dat" using 1:($4*200**(1/8)):($5*200**(1/8)) title "l=200" with yerrorbar