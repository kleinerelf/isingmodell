FC		 := gfortran
CFLAGS  := -Wall -Wline-truncation  -Wimplicit-interface -fwhole-file -fimplicit-none -std=f2008 -fopenmp
CPPFLAGS := -cpp
SRC	 := ${wildcard src/*.f08} \
		   ${wildcard src/utils/*.f08} \
		   ${wildcard src/modell/*.f08}
SRC_TEST := ${wildcard src/test/*_TEST.f08}
SRC_DEBUG:= ${wildcard src/debug/*_DEBUG.f08}

OBJ	 := $(subst .f08,.o,$(SRC))
TARGET   := main

check : CFLAGS += -g -Og  -Wcharacter-truncation -Wsurprising -Waliasing \
	-Wimplicit-interface  -Wunused-parameter -fcheck=all -fbacktrace -pedantic
debug : CFLAGS += -pg  -g -Og  -Wcharacter-truncation -Wsurprising -Waliasing \
	-Wimplicit-interface  -Wunused-parameter -fcheck=all -fbacktrace -pedantic
debug-all : CFLAGS += -pg -g -Og  -Wcharacter-truncation -Wsurprising -Waliasing \
	-Wimplicit-interface  -Wunused-parameter -fcheck=all -fbacktrace -pedantic
all : CFLAGS   += -O3


ifeq "$(MAKECMDGOALS)" "check"
CHECK		 = 1
TARGET_TEST := $(subst .f08,,$(SRC_TEST))
OBJ_TEST	:= $(subst .f08,.o,$(SRC_TEST))
# Remove target (main)
OBJ		:= $(subst .f08,.o, \
	$(filter-out $(addsuffix .f08, src/$(TARGET)), $(SRC)))
endif

ifeq "$(MAKECMDGOALS)" "debug"
DEBUG		 = 1
TARGET_DEBUG:= $(subst .f08,,$(SRC_DEBUG))
OBJ_DEBUG	:= $(subst .f08,.o,$(SRC_DEBUG))
# Remove target (main)
OBJ		:=  $(subst .f08,.o,$(filter-out $(addsuffix .f08, \
	src/$(TARGET)), $(SRC)))
endif

ifdef CHECK
$(TARGET_TEST): $(OBJ) $(OBJ_TEST)
	@echo "Building Test: $@..."
	$(FC) -o $@ $(OBJ) $(addsuffix .o, $@)
endif

ifdef DEBUG
$(TARGET_DEBUG): $(OBJ) $(OBJ_DEBUG)
	@echo "Building Debug: $@..."
	$(FC) -o $@ $(OBJ) $(addsuffix .o, $@)
endif

.PHONY: all check debug debug-all dist-clean clean
all: $(OBJ)
	@echo "Building $(TARGET)..."
	$(FC) $(CFLAGS) -o $(TARGET) $^

check: $(TARGET_TEST)

debug: $(TARGET_DEBUG)

debug-all: $(OBJ)
	@echo "Building $(TARGET)..."
	$(FC) $(CFLAGS) -o $(TARGET) $^

clean:
	@echo "Cleaning up mod-files"
	-rm -v *.mod
	@echo "Cleaning up src/... "
	-cd src; \
	rm -v *.o; \
	echo "Cleaning up src/*... ";\
	rm -v */*.o

dist-clean: clean
	-cd src; \
	echo "Cleaning up src/... "; \
	rm -v *.d; \
	rm -v *.tex; \
	echo "Cleaning up src/*... "; \
	rm -v */*.d; \
	rm -v */*.tex; \

%.o: %.f08
	$(FC) $(CFLAGS) -c -o $@ $<

%.d: %.f08
	@echo "Building dependencies for $<"
	@set -e; rm -f $@; \
	$(FC) -MM $(CPPFLAGS) $< > $@.tmp 2>/dev/null ; \
	sed 's,\($*\)\.o [ :]*,\1.o $@ ,g' < $@.tmp > $@ ; \
	rm -f $@.tmp

ifeq (,$(findstring clean,$(MAKEGOALS)))
ifeq (,$(findstring dist-clean,$(MAKEGOALS)))
ifdef CHECK
-include $(SRC_TEST:.f08=.d)
endif
ifdef DEBUG
-include $(SRC_DEBUG:.f08=.d)
endif
-include $(SRC:.f08=.d)
endif
endif
