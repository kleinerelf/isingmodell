#!/bin/bash

mkdir -p result

echo "Moving everthing to result/ ..."
mv *.dat result 2>&1 1>/dev/null
mv *.conf result 2>&1 1>/dev/null
mv *.err result 2>&1 1>/dev/null

echo "Open result/ ..."
cd result

echo "Plotting ..."
gnuplot plot.gpl

echo "Run pdflatex ..."
find . -name "*.tex" -exec pdflatex '{}' 1>/dev/null \;

echo "Cleaning up ..."
rm -f *.aux *.log *.eps *.tex
rm -f *-inc-eps-converted-to.pdf
